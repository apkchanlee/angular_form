import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class WindowService {

  get windowRef() {
    return window;
  }
  constructor(private router: Router) { console.log('inside Window Service'); }

  isLoggedIn() {
    return localStorage.getItem('mobile') ? true : false;
  }

  logout() {
    localStorage.removeItem('mobile');
    localStorage.clear();
    return this.router.navigate(['login']).finally();
  }
}