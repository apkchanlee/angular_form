import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { Location } from '@angular/common'
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ToastService } from '../toast.service';
import { environment } from 'src/environments/environment';
import { MouseEvent, MapsAPILoader } from '@agm/core';
import { google } from '@agm/core/services/google-maps-types';
import { Place } from 'aws-sdk/clients/location';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { LatLng } from 'ngx-google-places-autocomplete/objects/latLng';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  
  constructor(private location: Location, private http: HttpClient, private toast: ToastService, 
    private route: ActivatedRoute, private router: Router, @Inject(LOCALE_ID) public locale: string, 
    private _location: Location, private mapsAPILoader: MapsAPILoader) {
    
  }

  questions: any[] = [];
  options: any[] = [];
  answer: any = {lat: 13.0987328, lng: 80.2898647}; //13.0987328,80.2898647

  form_questions: any[] = [];
  form_questions_grouping: any[] = [];

  selectedIndex = 0;

  quest_type = 0;
  address = "";
  insert_option_key = "";
  insert_option_val = "";
  
  logo = environment.logo;

  sub_type_not_for: any = ["Ready to move", "Applied, Yet to receive", "No"];

  suspended = (environment.account_suspended_timer < (new Date()).getTime()/1000);

  ngOnInit(): void {
    this.refresh();
    
    this.route.queryParams.subscribe(
      params => {
         this.answer['phone'] = JSON.parse(params['phone']);
         console.log('Got param: ', this.answer);

      }
    )
  }

  public AddressChange(address: Address) {
    //setting address from API to local variable
    console.log(address);

    this.answer['address'] = address.formatted_address;
    this.answer['lat'] = address.geometry.location.lat();
    this.answer['lng'] = address.geometry.location.lng();
    
  }

  add(quest:any, event:any) {
    console.log(quest);
    console.log(event);

    this.answer[quest.question] = event;
  }

  add2(quest2:any, question:any, opt:any, event:any) {
    console.log(quest2);
    console.log(opt);
    console.log(event);

    if(!this.answer[opt]) {
      this.answer[opt] = [];
    }

    this.answer[opt][question] = event;

    console.log("this.answer", this.answer);
  }

  insert_option(option_id:any, option:any) {
    
    this.http.get(environment.apiBaseUrl+"insert.php?option_id="+option_id+"&option="+option).subscribe((data:any)=>{

      console.log(data);
      if(data.code == 200) {
        this.options = data.options;
        this.toast.showSuccess(option+" "+data.response);
        this.insert_option_key = "";
        this.insert_option_val = "";

      } else {
        this.toast.showWarning(data.response);
      }
    }); 
  }

  add_option(quest:any, event:any, type:any, multiselect: any) {
    console.log(quest);
    console.log(event);
    console.log(type);

    if(multiselect != "1") {
      
      this.answer[quest.question] = event;

      if(type && type != "") {
      
        this.quest_type = type;
        this.form_questions = this.questions.filter((row:any) => row.followup_id.split(",").includes("0") || row.followup_id.split(",").includes("4") || row.followup_id.split(",").includes(""+type));
        this.grouping();

      }

    } else {
      
        let myquest = "";

        if(this.answer[quest.question]) {
          myquest = this.answer[quest.question];
        }

        console.log(myquest);
        let arr = myquest.split(',');
        
        if(arr.includes(event)) {

          if(arr.length > 1) {

            const index: number = arr.indexOf(event);
            if (index !== -1) {
              arr.splice(index, 1);
            }     
          }
                
        } else {
          arr.push(event);
        }

        this.answer[quest.question] = arr.filter((n: any) => n).join(',');
    }
  }

  add_more() {
    
    this.form_questions = this.questions.filter((row:any) => row.followup_id.split(",").includes("0") || row.followup_id.split(",").includes("4") || row.followup_id.split(",").includes(""+this.quest_type));
    this.grouping();
  }
  
  next(question: any) {

    let valid = true;

    for(let mand of question.split(",")) {
      if(!this.answer[mand]) {
        valid = false;
        alert(mand+" Missing!");
      }
    }
    if(valid) {
      this.selectedIndex++;
    }
  }

  previous() {
    if(this.selectedIndex != 0) {
      this.selectedIndex--;
    } else {
          
      if(confirm("Are you sure you want to go back without submitting the form?")) {
        
        this._location.back();
      }
    }
  }

  submit() {
    console.log(this.answer);

    
    this.http.get(environment.apiBaseUrl+"answer.php?answer="+encodeURIComponent(JSON.stringify(this.answer))).subscribe((data:any)=>{

      console.log(data);
      this.toast.showSuccess("Form submitted successfully!");
    });
    
    this.router.navigate(['/thankyou'], { queryParams: { answer: JSON.stringify(this.answer) }});
  }

  grouping() {
    this.form_questions_grouping = [];
    let i = 0;
    
    console.log("question", this.form_questions);
    for(let quest of this.form_questions) {


      if(!this.form_questions_grouping[i]) {
        this.form_questions_grouping[i] = [];
      } 

      this.form_questions_grouping[i].push(quest);

      if(quest.type == "next") {

        i++;
      }
    }

    console.log("form questions grouping", this.form_questions_grouping);
    console.log("count", i);
    console.log("question", this.form_questions);
  }

  
  selectLocation($event: MouseEvent) {
    this.answer.lat = $event.coords.lat;
    this.answer.lng = $event.coords.lng;
    this.address = "";

    console.log($event);

    if($event.placeId) {
      const completeUrl = 'https://maps.googleapis.com/maps/api/geocode/json?place_id=' + $event.placeId + '&key=AIzaSyArcPuJ0bb9yOw__iK_7QvFcbzj3KgTNzI';

      
      this.http.get<any>(completeUrl).subscribe(data => {
        console.log(data.results);
        if(data.results[0].formatted_address) {
          this.address = data.results[0].formatted_address;
        }
      })        
      
    }
    console.log(this.answer);
  }

  refresh() {
    this.http.get(environment.apiBaseUrl+"data.php").subscribe((data:any)=>{
      console.log(data);
      this.questions = data.questions;
      this.options = data.options;

      this.form_questions = this.questions.filter((row:any) => row.followup_id.split(",").includes("0"));
      
      console.log(this.form_questions);
      this.grouping();
    });
  }

}
