import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common'
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-thank-you',
  templateUrl: './thank-you.component.html',
  styleUrls: ['./thank-you.component.scss']
})
export class ThankYouComponent implements OnInit {

  constructor(private route: ActivatedRoute, private _location: Location) { }

  answer: any;
  logo = environment.logo;

  suspended = (environment.account_suspended_timer < (new Date()).getTime()/1000);

  ngOnInit(): void {
    this.route.queryParams.subscribe(
      params => {
         let obj = JSON.parse(params['answer']);
         this.answer = Object.keys(obj).map(key => ({key: key, value: obj[key]}));
         console.log('Got param: ', this.answer);

      }
    )
  }

  
  previous() {
    if(confirm("Are you sure you want to go back to the form?")) {
      this._location.back();
    }
  }
}
