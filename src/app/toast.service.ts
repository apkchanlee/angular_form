import { Injectable } from '@angular/core';
import  { NgbToastService, NgbToastType, NgbToast }  from  'ngb-toast';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(private  toastService:  NgbToastService) { }


  toast: NgbToast | undefined;
  
  
	showSuccess(message:string): void {
		if(this.toast) {
			this.toastService.remove(this.toast);
		}
		this.toast = {
			toastType:  NgbToastType.Success,
			text:  message,
			timeInSeconds: 3,
			dismissible:  true,
			onDismiss: () => {
				console.log("Toast dismissed!!");
			}
		}
		this.toastService.show(this.toast);
	}
	showWarning(message:string): void {
		if(this.toast) {
			this.toastService.remove(this.toast);
		}
		this.toast = {
			toastType:  NgbToastType.Warning,
			text:  message,
			timeInSeconds: 3,
			dismissible:  true,
			onDismiss: () => {
				console.log("Toast dismissed!!");
			}
		}
		this.toastService.show(this.toast);
	}
	showDanger(message:string): void {
		if(this.toast) {
			this.toastService.remove(this.toast);
		}
		this.toast = {
			toastType:  NgbToastType.Danger,
			text:  message,
			timeInSeconds: 3,
			dismissible:  true,
			onDismiss: () => {
				console.log("Toast dismissed!!");
			}
		}
		this.toastService.show(this.toast);
	}
}
