import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ToastService } from '../toast.service';
import * as XLSX from 'xlsx';
import { Location } from '@angular/common'

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  answers: any = [];
  keys: any = {};
  access_granted = false;
  username: any;
  password: any;

  suspended = (environment.account_suspended_timer < (new Date()).getTime()/1000);
  
  logo = environment.logo;
  
  @ViewChild('table') table!: ElementRef;

  constructor(private http: HttpClient, private toast: ToastService, private _location: Location) { }

  ngOnInit(): void {
    
    this.http.get(environment.apiBaseUrl+"answer.php").subscribe((data:any)=>{

      this.answers = [];
      this.keys = [];
      for(let da of data) {

        console.log("da['answer']", da['answer'].replaceAll("\\", "/"));
        let obj = JSON.parse(da['answer'].replaceAll("\\", "/"));
        
        (Object.keys(obj).map(key => {

          if(!this.keys.includes(key)) {
            this.keys.push(key);
          }

          return {
            key: key, 
            value: obj[key]
          }
        }));

        this.answers.push(obj);
      }
      console.log("answers", this.answers);
    });

  }

  verify() {
    if(this.suspended) {

      this.toast.showWarning("Account suspended due to trial end. Please contact admin.");

    } else {

      if(this.username == "admin" && this.password == "qwerty") {
        this.access_granted = true;
      } else {
        this.toast.showWarning("Incorrect credentials");
      }

    }
  }

    
  ExportTOExcel()
  {
    const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.table.nativeElement);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    
    /* save to file */
    XLSX.writeFile(wb, 'Confidential Details.xlsx');
    
  }

  print() {
    window.print();
  }

  
  previous() {
    if(confirm("Are you sure you want to exit to previous page?")) {
      this._location.back();
    }
  }
}
