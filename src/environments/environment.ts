// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  account_suspended_timer: 1685989800,

  //apiBaseUrl: "https://infotechgalaxy.com/sample/api/",
  //logo: "assets/images/itg_logo.png",
  //name: "INFOTECH GALAXY <br> DATA FORM",

  apiBaseUrl: "http://gobrokerindia.com/emd/form/api/",
  logo: "assets/images/gobroker.png",
  name: "EMD PROJECT <br> DATA FORM",
  
  firebase: {      
    apiKey: "AIzaSyAjeXT1r85-DtYxNrQ0WdSC25HKy-pOD2Q",
    authDomain: "gobroker-form.firebaseapp.com",
    projectId: "gobroker-form",
    storageBucket: "gobroker-form.appspot.com",
    messagingSenderId: "303275719440",
    appId: "1:303275719440:web:658c43c082842d30bd0395",
    measurementId: "G-GPM93YR5WR"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
